package kk.orm.util;

import android.database.Cursor;

public class CursorUtils {

	public static <T> T getEntity(Cursor cursor, Class<T> clazz) {
		// 这里需要缓存Class的信息(主要是Field,不用每次都调用反射getDeclearedField())

		T entity = null;
		try {
			entity = clazz.newInstance();

			// 为Model赋值，每一列对应一个属性
			for (int column = 0; column < cursor.getColumnCount(); column++) {
				int type = cursor.getType(column);
				String name = cursor.getColumnName(column);
				String value = cursor.getString(column);

				FieldUtil.setValue(entity, name, value);// 在FieldUtil里面，统一判断field类型从String转换

				// if (type == Cursor.FIELD_TYPE_INTEGER)
				// FieldUtil.setValue(entity, name, cursor.getLong(column));
				// else if (type == Cursor.FIELD_TYPE_FLOAT)
				// FieldUtil.setValue(entity, name, cursor.getFloat(column));
				// else if (type == Cursor.FIELD_TYPE_STRING)
				// FieldUtil.setValue(entity, name, cursor.getString(column));
				// else if (type == Cursor.FIELD_TYPE_BLOB)
				// FieldUtil.setValue(entity, name, cursor.getBlob(column));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return entity;
	}
}
