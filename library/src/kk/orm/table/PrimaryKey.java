package kk.orm.table;


public class PrimaryKey extends Property {

	public static PrimaryKey valueOf(Property property) {

		PrimaryKey primaryKey = new PrimaryKey();
		primaryKey.setField(property.getField());
		primaryKey.setDataType(property.getDataType());
		primaryKey.setFieldName(property.getFieldName());

		return primaryKey;
	}
}
