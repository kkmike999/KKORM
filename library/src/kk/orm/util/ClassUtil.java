package kk.orm.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import kk.orm.table.TableInfo;

public class ClassUtil {

	/** 根据类，获取 数据表 名称 */
	public static <T> String getTableName(Class<T> clazz) {
		return TableInfo.getTableName(clazz);
	}

	/**
	 * 获取 此类（包括所有父类）的Field(非主键) <br>
	 * 
	 * @caution 此类会从{@linkplain TableInfo}获取缓存
	 * */
	public static <T> Field[] getDeclaredFields(Class<T> clazz) {
		List<Field> list = new ArrayList<Field>();

		// 缓存或获取缓存
		TableInfo table = TableInfo.get(clazz);

		// 获取从TableInfo缓存的Field[]
		Set<String> keys = table.propertyMap.keySet();
		for (String key : keys) {
			Field field = table.propertyMap.get(key).getField();
			list.add(field);
		}

		return list.toArray(new Field[0]);
	}
}
