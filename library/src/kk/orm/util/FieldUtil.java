package kk.orm.util;

import java.lang.reflect.Field;

import android.annotation.SuppressLint;

@SuppressLint("UseValueOf")
public class FieldUtil {

	/**
	 * 为对象成员赋值
	 * 
	 * @param entity
	 *            被储存的对象
	 * @param fieldName
	 *            成员名称
	 * @param value
	 *            属性的值(从数据表以String取出，在此方法内部再根据实体类 对应成员的类型，进行转换)
	 */
	public static void setValue(Object entity, String fieldName, String value) {
		try {
			Field field = getField(fieldName, entity.getClass());

			if (field != null && value != null) {
				Class<?> type = field.getType();
				String type_name = type.getName();

				if (!value.equals("null")) {
					if (type_name.equals("boolean") || type_name.equals("bool")) {
						if (value.equals("false")) {
							field.set(entity, false);
						} else if (value.equals("true")) {
							field.set(entity, true);
						}
					} else if (type.equals(Boolean.class)) {
						if (value.equals("false")) {
							field.set(entity, new Boolean(false));
						} else if (value.equals("true")) {
							field.set(entity, new Boolean(true));
						}
					} else if (type_name.equals("int") || type_name.equals(Integer.class.getName())) {
						field.set(entity, new Integer(value));
					} else if (type_name.equals("long") || type_name.equals(Long.class.getName())) {
						field.set(entity, new Long(value));
					} else if (type_name.equals("float") || type_name.equals(Float.class.getName())) {
						field.set(entity, new Float(value));
					} else if (type_name.equals("double") || type_name.equals(Double.class.getName())) {
						field.set(entity, new Double(value));
					} else if (type_name.equals("char") || type_name.equals(Character.class.getName())) {
						field.set(entity, new Character(value.charAt(0)));
					} else if (type_name.equals(String.class.getName())) {
						field.set(entity, new String(value));
					}

				} else if (type_name.equals(String.class.getName())) {
					field.set(entity, new String(value));
				}

				// else
				// field.set(entity, value);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取 fieldName元素的值
	 * 
	 * @param entity
	 * 
	 * @param fieldName
	 *            元素名称
	 * */
	public static Object getValue(Object entity, String fieldName) {
		Object obj = "";
		try {
			Field field = getField(fieldName, entity.getClass());

			if (field != null)
				obj = field.get(entity);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return obj;
	}

	/**
	 * 获取所有权限的Field，并且accessible.
	 * 
	 * @param fieldName
	 *            成员名称
	 * @return Field which is accessible.<br>
	 *         可以进行{@link Field#get(Object)} & {@link Field#set(Object, Object)}等操作,即使是private
	 */
	private static Field getField(String fieldName, Class<?> clazz) {
		Field field = null;

		Field[] fields = ClassUtil.getDeclaredFields(clazz);
		for (Field f : fields) {
			if (f.getName().equals(fieldName)) {
				field = f;
			}
		}

		return field;
	}

	/**
	 * 获取Field的sql类型（暂时未能获取泛型类型 ）
	 * 
	 * 注意:android的sql是不支持boolean类型的
	 */
	public static String getSqlType(Field field) {
		Class<?> type = null;
		if (field != null)
			type = field.getType();

		try {
			if (type != null) {
				String type_name = type.getName();

				if (type_name.equals("int") || type_name.equals(Integer.class.getName())) {
					return "INT";
				} else if (type_name.equals("long") || type_name.equals(Long.class.getName())) {
					return "LONG";
				} else if (type_name.equals("float") || type_name.equals(Float.class.getName())) {
					return "FLOAT";
				} else if (type_name.equals("double") || type_name.equals(Double.class.getName())) {
					return "Double";
				} else if (type_name.equals("bool") || type_name.equals("boolean") || type_name.equals(Boolean.class.getName())) {// sql不支持boolean
					return "VARCHAR(6)";
				} else if (type_name.equals("char") || type_name.equals(Character.class.getName())) {
					return "CHAR(3)";// 不知道汉子占多少字符
				} else if (type_name.equals(String.class.getName())) {
					return "VARCHAR(255)";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "VARCHAR(255)";// 默认为字符串
	}
}
