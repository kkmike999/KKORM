#KKORM

KKORM交流平台 Q群：249930255
KKORM介绍：
    KKORM是一个Android sqlite orm框架。KKORM宗旨是：简洁、快速。代码少最尽量多的事情。


KKORM使用方法：

我们要储存的类Entity，里面只能放基本数据类型(int、float、double、string、char、boolean，暂时不支持Time & Date类型，之后会拓展)，放其他(例如Map、List之类)会报错噢~


public class Entity {

    //默认主键，自增。最好有id主键，便于debug和逻辑判断。暂不支持自定义主键
    int				id;

	public boolean	base_boolean;
	public double	base_double;
	public float	base_float;
	public int		base_int;
	public long		base_long;

	public Boolean	mBoolean;
	public Double	mDouble;
	public Float	mFloat;
	public Integer	mInteger;
	public Long		mLong;
}

在Activity


初始化：
   
    private DB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.activity_kkorm);
       
    	db = DB.getInstance(this);
    }
    
保存操作：
    
    Entity entity = new Entity();
    entity.base_boolean = true;
    entity.base_double = 10.2;
    entity.base_float = 9.1f;
    entity.base_int = 99;
    entity.base_long = System.currentTimeMillis();

    entity.mBoolean = new Boolean(true);
    entity.mDouble = new Double(10.2);
    entity.mFloat = new Float(9.1);
    entity.mInteger = new Integer(99);
    entity.mLong = new Long(System.currentTimeMillis() / 1000);

    db.save(entity);

查询：

    List<Entity> list = db.findAll(Entity.class);
    
删除：

    1.db.delete(entity); //以entity主键作为where执行删除操作
    2.db.deleteAll(Entity.class);
    3.db.deleteByWhere(Entity.class, "id=1");

更新:

    List<Entity> list = db.findAll(Entity.class);
    if (list.size() > 0) {
		Entity entity = list.get(0);
		entity.base_int = 9999999;
	
        // db.update(Entity, "id=1");
		db.update(entity); //以entity主键作为where执行更新操作
	}
    
删除表：

    db.dropTable(Entity.class);