package com.kkorm.data;

import android.annotation.SuppressLint;

@SuppressLint("UseValueOf")
public class Entity {

	/**
	 * 默认主键，自增。最好有id主键，便于debug和逻辑判断。
	 * 
	 * 暂不支持自定义主键
	 */
	int				id;

	public boolean	base_boolean;
	public double	base_double;
	public float	base_float;
	public int		base_int;
	public long		base_long;

	public Boolean	mBoolean;
	public Double	mDouble;
	public Float	mFloat;
	public Integer	mInteger;
	public Long		mLong;
}
