package com.kkorm.activity;

import java.util.List;

import com.example.kkorm_demo.R;
import com.kkorm.data.Entity;

import kk.orm.DB;
import kk.orm.util.TimeUtil;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class KKORM_Activity extends Activity implements OnClickListener {

	private static final String	TAG		= KKORM_Activity.class.getName();
	private Context				mContext;
	private Handler				handler	= new Handler();
	private DB					db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kkorm);

		mContext = this;

		findViewById(R.id.btn_save).setOnClickListener(this);
		findViewById(R.id.btn_drop_table).setOnClickListener(this);
		findViewById(R.id.btn_del).setOnClickListener(this);
		findViewById(R.id.btn_update).setOnClickListener(this);
		findViewById(R.id.btn_findAll).setOnClickListener(this);
		findViewById(R.id.btn_insert_stress).setOnClickListener(this);
		findViewById(R.id.btn_limite).setOnClickListener(this);

		db = DB.getInstance(mContext);
	}

	@SuppressLint("UseValueOf")
	@Override
	public void onClick(View v) {

		Entity entity = new Entity();
		List<Entity> all = null;

		switch (v.getId()) {
			case R.id.btn_save:// 插入操作

				entity.base_boolean = true;
				entity.base_double = 10.2;
				entity.base_float = 9.1f;
				entity.base_int = 99;
				entity.base_long = System.currentTimeMillis();

				entity.mBoolean = new Boolean(true);
				entity.mDouble = new Double(10.2);
				entity.mFloat = new Float(9.1);
				entity.mInteger = new Integer(99);
				entity.mLong = new Long(System.currentTimeMillis() / 1000);

				db.save(entity);

				break;

			case R.id.btn_del:// 删除操作

				new Thread(new Runnable() {

					@Override
					public void run() {
						List<Entity> list = db.findAll(Entity.class);
						if (list != null && list.size() > 0) {
							Entity entity = list.get(0);
							db.delete(entity);
						}

						// db.deleteAll(Entity.class);
						// db.deleteByWhere(Entity.class, "");
					}
				}).start();

				break;
			case R.id.btn_update:// 更新操作

				// 防止插入1000条之后，查询缓慢导致卡死UI进程
				new Thread(new Runnable() {

					@Override
					public void run() {
						List<Entity> list = db.findAll(Entity.class);
						if (list.size() > 0) {
							Entity entity = list.get(0);
							entity.base_int = 23482374;

							// db.update(Entity, "id=1");
							db.update(entity);
						}
					}
				}).start();

				break;

			case R.id.btn_findAll:// 查询操作

				new Thread(new Runnable() {

					@Override
					public void run() {
						Log.d(TAG, "开始查询");

						TimeUtil.begin();

						List<Entity> list = db.findAllByWhere(Entity.class, "id!=0 limit 50");

						TimeUtil.end();

						String msg = "查询" + list.size() + "条耗时: " + TimeUtil.time();
						Toast(msg);
						Log.d(TAG, msg);
					}

				}).start();

				break;

			case R.id.btn_drop_table:// 删除表

				db.dropTable(Entity.class);
				break;

			case R.id.btn_insert_stress:// 压力测试

				new Thread(new Runnable() {

					@Override
					public void run() {
						Entity entity = new Entity();
						Log.d(TAG, "开始插入");

						TimeUtil.begin();

						// 开始事务。提高效率(不用事务，插入1000条数据可能要十几秒，用事务可能不到1秒，具体要看类是否复制)
						db.beginTrancation();

						for (int i = 0; i < 1000; i++) {
							db.save(entity);
						}

						db.endTrancation();

						TimeUtil.end();

						String msg = "插入1000条耗时: " + TimeUtil.time();
						Toast(msg);
						Log.d(TAG, msg);
					}
				}).start();

				break;

			case R.id.btn_limite:

				break;

			default:
				break;
		}
	}

	private void Toast(final String msg) {
		handler.post(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(mContext, msg, 0).show();
			}
		});
	}
}
