package kk.orm.table;

import java.lang.reflect.Field;

/**
 * @title 属性
 * @description 【非主键】的【基本数据类型】 都是属性
 * @author michael Young (www.YangFuhai.com)
 * @version 1.0
 * @created 2012-10-10
 */
public class Property {

	private String		fieldName;
	// private String column;
	// private String defaultValue;
	private Class<?>	dataType;
	private Field		field;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Class<?> getDataType() {
		return dataType;
	}

	public void setDataType(Class<?> dataType) {
		this.dataType = dataType;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

}
