package com.kkorm.util;

public class TimeUtil {

	private static long	start;
	private static long	end;

	public static void begin() {
		start = System.currentTimeMillis();
		// System.out.println("开始时间：" + start);
	}

	public static void end() {
		end = System.currentTimeMillis();
		// System.out.println("结束时间：" + end);
	}

	public static long time() {
		return (end - start);
	}
}
